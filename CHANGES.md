# Changelog

## 0.0.19 (unreleased)

-   No changes yet.

## 0.0.18 (2019-02-25)

-   Re-worded part of the EM_COINC circulars that gives the time difference
    between the neutrino/GRB trigger and GW candidate event.

## 0.0.17 (2019-02-25)

-   Fixed an unfortunate typo.

## 0.0.16 (2019-02-25)

-   Improved the EM_COINC and initial circulars.

## 0.0.15 (2019-02-22)

-   Added template for EM_COINC circulars, i.e., circulars produced when
    RAVEN finds a coincidence between GW and external triggers.

## 0.0.14 (2019-02-13)

-   Added template for retraction circulars.

## 0.0.13.dev0 (2019-02-13)

-   Update the XPath expression for obtaining the sky map URL to conform to the
    new schema for O3. This should fix the missing sky map paragraphs in
    circulars that are uploaded to GraceDb.

## 0.0.12 (2019-02-12)

-   Circulars include p_astro information when available.

## 0.0.11 (2018-08-02)

-   Circulars are generated with uncertainty ellipse text when they are a good
    approximation.

## 0.0.10 (2018-06-28)

-   Changed how VOEvent text is pulled down; now using `client.files()` method
    versus relying on `client.voevents()` json response.

## 0.0.9 (2018-06-28)

-   Circulars are generated strictly for superevents.

## 0.0.8 (2018-06-27)

-   GraceDb links in circulars now reflect the URL of the GraceDb server from
    which the event originated.

## 0.0.7 (2018-05-24)

-   Drop support for Python 2.

-   Do not print the circular if calling `compose()` from a Python script.

-   Remove duplicate skymaps and keep only the lowest latency ones.

-   Do not include skymaps produced outside the LVC.

-   Optional GraceDb client keyword argument when calling compose().

## 0.0.6 (2018-05-08)

-   Make it easier to call functions from Python scripts, like this:

        >>> from ligo import followup_advocate
        >>> text = followup_advocate.compose('G299232')

-   Add `--service` command line option to set the GraceDB service URL.

## 0.0.5 (2018-05-03)

-   First version released on PyPI.
